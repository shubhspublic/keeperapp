import React, { useState } from "react";
import Header from "./Header.jsx";
import Note from "./Note";
import Footer from "./Footer.jsx";
import CreateArea from "./createArea";

function App() {

  function uuidv4() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        var r = (Math.random() * 16) | 0,
          v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      }
    );
  }

  const [notes, setNotes] = useState([
    { key: uuidv4(), title: "Note title", content: "Note content" },
  ]);

  function deleteNote(key) {
    setNotes((prevNotes) => {
      return prevNotes.filter((items, index) => {
        return items.key !== key;
      });
    });
  }

  function createNote(notes) {
    console.log("createNote --> ", notes);
    return (
      <Note
        title={notes.title}
        content={notes.content}
        deleteItem={deleteNote}
        keyId={notes.key}
      />
    );
  }

  function checkNotes(keep) {
    setNotes((prevNotes) => {
      return [...prevNotes, keep];
    });
    return notes.map(createNote);
  }

  return (
    <div>
      <Header />
      <CreateArea sendNotes={checkNotes} />
      {notes.map(createNote)}
      <Footer />
    </div>
  );
}

export default App;
