import React, { useState } from "react";

function CreateArea(props) {
  const [_title, setTitle] = useState("");
  const [_content, setContent] = useState("");

  let keep = {
    key: uuidv4(),
    title: _title,
    content: _content,
  };

  function onChangeTitle(event) {
    setTitle(event.target.value);
  }

  function onChangeContent(event) {
    setContent(event.target.value);
  }

  function uuidv4() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        var r = (Math.random() * 16) | 0,
          v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      }
    );
  }

  return (
    <div>
      <form>
        <input
          name="title"
          onChange={onChangeTitle}
          placeholder="Title"
          value={_title}
        />
        <textarea
          name="content"
          onChange={onChangeContent}
          placeholder="Take a note..."
          rows="3"
          value={_content}
        />
        <button
          type="submit"
          onClick={(event) => {
            props.sendNotes(keep);
            event.preventDefault();
            setTitle("")
            setContent("")
          }}
        >
          Add
        </button>
      </form>
    </div>
  );
}

export default CreateArea;
